import * as firebase from "firebase";
import "firebase/database";

let config = {
  apiKey: "AIzaSyBpPR3_oqddVEDT3IQDulB5EKGCpKIGMQY",
  authDomain: "encryptgame-2956b.firebaseapp.com",
  databaseURL: "https://encryptgame-2956b.firebaseio.com",
  projectId: "encryptgame-2956b",
  storageBucket: "encryptgame-2956b.appspot.com",
  messagingSenderId: "681860365322",
  appId: "1:681860365322:web:c0a57d0367986c865bf3f3"
};

firebase.initializeApp(config);

export default firebase.database();
