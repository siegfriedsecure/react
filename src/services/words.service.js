import firebase from "../firebase";

const db = firebase.ref("/words");

class WordsDataService {
  getAll() {
    return db;
  }

  create(words) {
    return db.push(words);
  }

  update(key, value) {
    return db.child(key).update(value);
  }

  delete(key) {
    return db.child(key).remove();
  }

  deleteAll() {
    return db.remove();
  }
}

export default new WordsDataService();
