import React, { Component } from "react";
import WordsDataService from "../services/words.service";

import Word from "./random-words.component";

export default class WordsList extends Component {
  constructor(props) {
    super(props);
    this.refreshList = this.refreshList.bind(this);
    this.setActiveWord = this.setActiveWord.bind(this);
    this.removeAllWords = this.removeAllWords.bind(this);
    this.onDataChange = this.onDataChange.bind(this);

    this.state = {
      words: [],
      currentWord: null,
      currentIndex: -1,
    };
  }

  componentDidMount() {
    WordsDataService.getAll().on("value", this.onDataChange);
  }

  componentWillUnmount() {
    WordsDataService.getAll().off("value", this.onDataChange);
  }

  onDataChange(items) {
    let words = [];

    items.forEach((item) => {
      let key = item.key;
      let data = item.val();
      words.push({
        key: key,
        word1: data.word1,
        word2: data.word2,
        word3: data.word3,
        word4: data.word4,
        published: data.published,
      });
    });

    this.setState({
        words: words,
    });
  }

  refreshList() {
    this.setState({
      currentWord: null,
      currentIndex: -1,
    });
  }

  setActiveWord(word, index) {
    this.setState({
      currentWord: word,
      currentIndex: index,
    });
  }

  removeAllWords() {
    WordsDataService.deleteAll()
      .then(() => {
        this.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { words, currentWord, currentIndex } = this.state;

    return (
      <div className="list row">
        <div className="col-md-6">
          <h4>詞組</h4>
          <h4>Words</h4>
          <ul className="list-group">
            {words &&
              words.map((word, index) => (
                <li
                  className={
                    "list-group-item " +
                    (index === currentIndex ? "active" : "")
                  }
                  onClick={() => this.setActiveWord(word, index)}
                  key={index}
                >
                  {word.key}
                </li>
              ))}
          </ul>

          <button
            className="m-3 btn btn-sm btn-danger"
            onClick={this.removeAllWords}
          >
            全部移除 - Remove All
          </button>
        </div>
        <div className="col-md-6">
          {currentWord ? (
            <Word
              word={currentWord}
              refreshList={this.refreshList}
            />
          ) : (
            <div>
              <br />
              <p>請點選你的詞組，不要點錯個 - Please click on your words, don’t click the wrong one</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}
