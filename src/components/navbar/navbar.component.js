import React, { Component } from "react";
import { Switch, Route, Link } from "react-router-dom";


import AddTutorial from "../add-tutorial.component";
import TutorialsList from "../tutorials-list.component";
import RandomWordsGenerate from "../random-word-generate.component";
import WordsList from "../words-list.component";
import Home from "../home.component";
import GenerateCode from  "../generate-code.component";
// import SignUpButton from "../login/signup-button.component";
// import AuthButton from "../login/authentication-button.component";
import Profile from  "../user/profile";
import  Loading  from "../loader/loading.component";

export default class Navbar extends Component {
    
    render() {
        return ( 
            <div>
                <nav className="navbar navbar-expand navbar-dark bg-dark">
                <a href="/home" className="navbar-brand">
                    截碼戰
                </a>
                <div className="navbar-nav mr-auto">
                    <li className="nav-item">
                    <Link to={"/words"} className="nav-link">
                        詞組
                    </Link>
                    </li>
                    <li className="nav-item">
                    <Link to={"/generateWords"} className="nav-link">
                        抽詞組
                    </Link>
                    </li>
                    <li className="nav-item">
                    <Link to={"/generateCode"} className="nav-link">
                        抽密碼
                    </Link>
                    </li>
                    {/* <li className="nav-item">
                    <Link to={"/profile"} className="nav-link">
                        Profile
                    </Link>
                    </li>
                    <li>
                        <AuthButton/>
                    </li>
                    <li>
                        <SignUpButton/>
                    </li>
                    <li className="nav-item">
                    <Link to={"/loading"} className="nav-link">
                        Loading
                    </Link>
                    </li> */}
                </div>
                </nav>

                <div className="containerWrapper">
                <p className="titlePage">React</p>
                
                <Switch>
                    <Route exact path={["/tutorials"]} component={TutorialsList} />
                    <Route exact path="/add" component={AddTutorial} />
                    <Route exact path={["/words"]} component={WordsList} />
                    <Route exact path="/generateWords" component={RandomWordsGenerate} />
                    <Route exact path={["/generateCode"]} component={GenerateCode} />
                    <Route exact path={["/profile"]} component={Profile} />
                    <Route exact path={["/"]} component={Home} />
                    <Route exact path={["/loading"]} component={Loading} />
                </Switch>
                </div>
            </div>
        );
    }
}
    