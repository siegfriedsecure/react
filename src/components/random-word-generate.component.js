import React, { Component } from "react";

import WordsDataService from "../services/words.service";
import Translate from "../libs/translate";
// var translate = require('./lib/translate')

export default class RandomWordsGenerate extends Component {
  constructor(props) {
    super(props);
    this.generateWord = this.generateWord.bind(this);
    this.goToList = this.goToList.bind(this);

    this.state = {
      word1: "", 
      word2: "",
      word3: "",
      word4: "",
      published: false,

      submitted: false,
    };
  }

  generateWord() {
    var randomWords =require('random-words');
    var Sentencer = require('sentencer');
    var input = 'English', output = "Chinese";
    var text = "food";
    var text2 = "";
    // Translate.text({input:input,output:output}, text, function(err, text2){
    //   console.log(text2);
    // }

    // let wordList = randomWords(4);
    // var noun = Sentencer.make("{{ noun }}");
    let data = {
        word1: Sentencer.make("{{ noun }}"),
        word2: Sentencer.make("{{ noun }}"),
        word3: Sentencer.make("{{ noun }}"),
        word4: Sentencer.make("{{ noun }}"),
        published: false
    };

    WordsDataService.create(data)
      .then(() => {
        console.log("Created new words successfully!");
        this.setState({
          submitted: true,
        });
      })
      .catch((e) => {
        console.log(e);
      });
  }

  goToList() {
    window.location.href = 'words';
  }

  render() {
    return (
      <div className="submit-form">
        {this.state.submitted ? (
          <div>
            <h4>點選下面按鈕查看你的詞組</h4>
            <h4>Click the button below to view your words</h4>
            <button className="btn btn-success" onClick={this.goToList}>
              查看詞組 - View phrase
            </button>
          </div>
        ) : (
          <div>
            <button onClick={this.generateWord} className="btn btn-success">
              抽詞組 - Extract words
            </button>
          </div>
        )}
      </div>
    );
  }
}
