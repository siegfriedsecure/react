import React, { Component } from "react";
import WordsDataService from "../services/words.service";

export default class Word extends Component {
  constructor(props) {
    super(props);
    this.deleteWord = this.deleteWord.bind(this);

    this.state = {
      currentWord: {
        key: null,
        word1: "",
        word2: "",
        word3: "",
        word4: "",
        published: false,
      },
      message: "",
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    const { word } = nextProps;
    if (prevState.currentWord.key !== word.key) {
      return {
        currentWord: word,
        message: ""
      };
    }

    return prevState.currentWord;
  }

  componentDidMount() {
    this.setState({
        currentWord: this.props.word,
    });
  }

 

  deleteWord() {
    WordsDataService.delete(this.state.currentWord.key)
      .then(() => {
        this.props.refreshList();
      })
      .catch((e) => {
        console.log(e);
      });
  }

  render() {
    const { currentWord } = this.state;

    return (
      <div>
        <h4>Words</h4>
        {currentWord ? (
          <div className="edit-form">
            <form>
              <div className="form-group">
                <label htmlFor="title">Word 1</label>
                <input
                  type="text"
                  className="form-control"
                  id="word1"
                  value={currentWord.word1}
                  readOnly
                />
              </div>
              <div className="form-group">
                <label htmlFor="title">Word 2</label>
                <input
                  type="text"
                  className="form-control"
                  id="word2"
                  value={currentWord.word2}
                  readOnly
                />
              </div>
              <div className="form-group">
                <label htmlFor="title">Word 3</label>
                <input
                  type="text"
                  className="form-control"
                  id="word3"
                  value={currentWord.word3}
                  readOnly
                />
              </div>
              <div className="form-group">
                <label htmlFor="title">Word 4</label>
                <input
                  type="text"
                  className="form-control"
                  id="word4"
                  value={currentWord.word4}
                  readOnly
                />
              </div> 
              
            </form>

            <button
              className="badge badge-danger mr-2"
              onClick={this.deleteWord}
            >
              Delete
            </button>

            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <br />
            <p>Please click on a Word...</p>
          </div>
        )}
      </div>
    );
  }
}
