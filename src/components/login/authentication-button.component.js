// src/components/authentication-button.js

import React from "react";

import LoginButton from "../login/login-button.component";
import LogoutButton from "../login/logout-button.component";

import { useAuth0 } from "@auth0/auth0-react";

const AuthenticationButton = () => {
  const { isAuthenticated } = useAuth0();
  console.log(useAuth0());
  return isAuthenticated ? <LogoutButton /> : <LoginButton />;
};

export default AuthenticationButton;