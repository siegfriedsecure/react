import React, { Component } from "react";


export default class GenerateCode extends Component {
  constructor(props) {
    super(props);
    this.generateCode = this.generateCode.bind(this);

    this.state = {
      randomizedNumber: [],
    };
  }

  generateCode() {
    let arrCode = [1, 2, 3, 4];

    let blo = this.shuffleArray(arrCode);
    // let removed = Math.floor(Math.random() * 4); 
    this.setState({
        randomizedNumber: blo.splice(1,4),
      });
    this.render();
  }

  shuffleArray(array) {
    let i = array.length - 1;
    for (; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      const temp = array[i];
      array[i] = array[j];
      array[j] = temp;
    }
    return array;
  }
  removeRandom(array) {
    while(array.length){
    const random = Math.floor(Math.random() * array.length);
    const el = array.splice(random, 1)[0];
    return el;
    }
  }


  render() {
    // let thinning = removeRandom(this.state.randomizedNumber);
    let codeString = this.state.randomizedNumber.join();
    return (
      <div className="submit-form">
        {this.state.randomizedNumber.length>0 ? (
          <div className="codeNumberWrapper">
            <h4>你的密碼為 - Your secret code is：</h4>
            <p className="codeNumber">{codeString}</p>
            <button className="btn btn-success" onClick={this.generateCode}>
              再抽一次 - Draw again
            </button>
          </div>
        ) : (
          <div>
            <button onClick={this.generateCode} className="btn btn-success">
              抽密碼 -Draw code
            </button>
          </div>
        )}
      </div>
    );
  }
}
